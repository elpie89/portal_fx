﻿Shader "Unlit/Holograms" {
	SubShader{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+2" }

		Pass{
		ZTest Greater
		Stencil{
			Ref 1
			Comp Equal
			ZFail DecrWrap
		}
		CGPROGRAM
		#include "UnityCG.cginc"
		#pragma vertex vert
		#pragma fragment frag
		struct appdata {
		float4 vertex : POSITION;
	};
	struct v2f {
		float4 pos : SV_POSITION;
	};
	v2f vert(appdata v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		return o;
	}
	half4 frag(v2f i) : SV_Target{
		return half4(0,0,1,1);
	}
		ENDCG
	}

		Pass{
		ZTest Less
		/*Stencil{
		Ref 0
		Comp NotEqual

		}*/
		
		CGPROGRAM
#include "UnityCG.cginc"
#pragma vertex vert
#pragma fragment frag
		struct appdata {
		float4 vertex : POSITION;
	};
	struct v2f {
		float4 pos : SV_POSITION;
	};
	v2f vert(appdata v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		return o;
	}
	half4 frag(v2f i) : SV_Target{
		return half4(1,0,0,1);
	}
		ENDCG
	}
	}
}
